/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Entidad.Usuario;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author neuron
 */
@Controller
public class Controlador {

    Conexion con = new Conexion();
    JdbcTemplate jdbcTemplate = new JdbcTemplate(con.Conectar());
    ModelAndView mav = new ModelAndView();
    String CVE_USUA;
    List datos;

    @RequestMapping("index.htm")
    public ModelAndView Listar() {
        String sql = "select * from c_usuarios";
        datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("listUser");
        return mav;
    }

    @RequestMapping(value = "newUser.htm", method = RequestMethod.GET)
    public ModelAndView Agregar() {
        mav.addObject(new Usuario());
        mav.setViewName("agregar");
        return mav;
    }

    @RequestMapping(value = "newUser.htm", method = RequestMethod.POST)
    public ModelAndView Agregar(Usuario u) {
        String sql = "insert into c_usuarios (CVE_USUA, NOM_USUA, APP_USUA, APM_USUA, RFC_USUA, DES_PUESTO, PASSWORD_USUA,"
                + "EMAIL_USUA, TEL_USUA, NUM_EMPLEADO_USUA, FEC_ALT, FECH_BAJ, FECH_ULT_MOD, CVE_PERFIL,"
                + "PID_USUA, CON_USUA, PROVEEDOR)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        this.jdbcTemplate.update(sql, u.getCVE_USUA(), u.getNOM_USUA(), u.getAPP_USUA(), u.getAPM_USUA(), u.getRFC_USUA(), u.getDES_PUESTO(), 
                u.getPASSWORD_USUA(), u.getEMAIL_USUA(), u.getTEL_USUA(), u.getNUM_EMPLEADO_USUA(), u.getFECH_ALT(), u.getFECH_BAJ(),
                u.getFECH_ULT_MOD(), u.getCVE_PERFIL(), u.getPID_USUA(), u.getCON_USUA(), u.getPROVEEDOR());
        return new ModelAndView("redirect:/index.htm");
    }

    @RequestMapping(value = "editUser.htm", method = RequestMethod.GET)
    public ModelAndView Editar(HttpServletRequest request) {

        CVE_USUA = request.getParameter("id");
        String sql = "select * from c_usuarios where CVE_USUA=" + CVE_USUA;
        datos = this.jdbcTemplate.queryForList(sql);
        mav.addObject("lista", datos);
        mav.setViewName("editar");
        return mav;
    }

    @RequestMapping(value = "editUser.htm", method = RequestMethod.POST)
    public ModelAndView Editar(Usuario u) {
        String sql = "UPDATE c_usuarios SET NOM_USUA=?, APP_USUA=?, APM_USUA=?, RFC_USUA=?, DES_PUESTO=?, PASSWORD_USUA=?,"
                + "EMAIL_USUA=?, TEL_USUA=?, NUM_EMPLEADO_USUA=?, FEC_ALT=?, FECH_BAJ=?, FECH_ULT_MOD=?, CVE_PERFIL=?,"
                + "PID_USUA=? CON_USUA=?, PROVEEDOR=? WHERE CVE_USUA=" + CVE_USUA;
        this.jdbcTemplate.update(sql, u.getNOM_USUA(), u.getAPP_USUA(), u.getAPM_USUA(), u.getRFC_USUA(), u.getDES_PUESTO(), 
                u.getPASSWORD_USUA(), u.getEMAIL_USUA(), u.getTEL_USUA(), u.getNUM_EMPLEADO_USUA(), u.getFECH_ALT(), u.getFECH_BAJ(),
                u.getFECH_ULT_MOD(), u.getCVE_PERFIL(), u.getPID_USUA(), u.getCON_USUA(), u.getPROVEEDOR());
        return new ModelAndView("redirect:/index.htm");

    }

    @RequestMapping("deleteUser.htm")
    public ModelAndView Delete(HttpServletRequest request) {

        CVE_USUA = request.getParameter("CVE_USUA");
        String sql = "delete from c_usuarios where Id=" + CVE_USUA;
        this.jdbcTemplate.update(sql);
        return new ModelAndView("redirect:/index.htm");
    }
}