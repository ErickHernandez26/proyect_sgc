/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

import java.sql.Date;

/**
 *
 * @author ErickVonVilthem
 */
public class Opcion {
    
    String CVE_OPCION;
    String DES_OPCION;
    String USUARIO_ALTA;
    String USUARIO_BAJA;
    String USUARIO_MOD;
    Date FECHA_ALTA;
    Date FECHA_BAJA;
    Date FECHA_MOD;

    public Opcion() {
    }

    public Opcion(String CVE_OPCION, String DES_OPCION, String USUARIO_ALTA, String USUARIO_BAJA, String USUARIO_MOD, Date FECHA_ALTA, Date FECHA_BAJA, Date FECHA_MOD) {
        this.CVE_OPCION = CVE_OPCION;
        this.DES_OPCION = DES_OPCION;
        this.USUARIO_ALTA = USUARIO_ALTA;
        this.USUARIO_BAJA = USUARIO_BAJA;
        this.USUARIO_MOD = USUARIO_MOD;
        this.FECHA_ALTA = FECHA_ALTA;
        this.FECHA_BAJA = FECHA_BAJA;
        this.FECHA_MOD = FECHA_MOD;
    }

    public String getCVE_OPCION() {
        return CVE_OPCION;
    }

    public void setCVE_OPCION(String CVE_OPCION) {
        this.CVE_OPCION = CVE_OPCION;
    }

    public String getDES_OPCION() {
        return DES_OPCION;
    }

    public void setDES_OPCION(String DES_OPCION) {
        this.DES_OPCION = DES_OPCION;
    }

    public String getUSUARIO_ALTA() {
        return USUARIO_ALTA;
    }

    public void setUSUARIO_ALTA(String USUARIO_ALTA) {
        this.USUARIO_ALTA = USUARIO_ALTA;
    }

    public String getUSUARIO_BAJA() {
        return USUARIO_BAJA;
    }

    public void setUSUARIO_BAJA(String USUARIO_BAJA) {
        this.USUARIO_BAJA = USUARIO_BAJA;
    }

    public String getUSUARIO_MOD() {
        return USUARIO_MOD;
    }

    public void setUSUARIO_MOD(String USUARIO_MOD) {
        this.USUARIO_MOD = USUARIO_MOD;
    }

    public Date getFECHA_ALTA() {
        return FECHA_ALTA;
    }

    public void setFECHA_ALTA(Date FECHA_ALTA) {
        this.FECHA_ALTA = FECHA_ALTA;
    }

    public Date getFECHA_BAJA() {
        return FECHA_BAJA;
    }

    public void setFECHA_BAJA(Date FECHA_BAJA) {
        this.FECHA_BAJA = FECHA_BAJA;
    }

    public Date getFECHA_MOD() {
        return FECHA_MOD;
    }

    public void setFECHA_MOD(Date FECHA_MOD) {
        this.FECHA_MOD = FECHA_MOD;
    }
    
    
}
