/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

import java.sql.Date;

/**
 *
 * @author ErickVonVilthem
 */
public class Perfil {
    
    String CVE_PERFIL;
    String DES_PERFIL;
    String CVE_PERFIL_PADRE;
    Date FEC_ALT;
    String CVE_USU_ALTA;
    Date FEC_BAJ;
    String CVE_USU_BAJ;
    Date FEC_ULT_MOD;
    String CVE_USUA;
    String CVE_SERVICIOS;

    public Perfil() {
    }

    public Perfil(String CVE_PERFIL, String DES_PERFIL, String CVE_PERFIL_PADRE, Date FEC_ALT, String CVE_USU_ALTA, Date FEC_BAJ, String CVE_USU_BAJ, Date FEC_ULT_MOD, String CVE_USUA, String CVE_SERVICIOS) {
        this.CVE_PERFIL = CVE_PERFIL;
        this.DES_PERFIL = DES_PERFIL;
        this.CVE_PERFIL_PADRE = CVE_PERFIL_PADRE;
        this.FEC_ALT = FEC_ALT;
        this.CVE_USU_ALTA = CVE_USU_ALTA;
        this.FEC_BAJ = FEC_BAJ;
        this.CVE_USU_BAJ = CVE_USU_BAJ;
        this.FEC_ULT_MOD = FEC_ULT_MOD;
        this.CVE_USUA = CVE_USUA;
        this.CVE_SERVICIOS = CVE_SERVICIOS;
    }

    public String getCVE_PERFIL() {
        return CVE_PERFIL;
    }

    public void setCVE_PERFIL(String CVE_PERFIL) {
        this.CVE_PERFIL = CVE_PERFIL;
    }

    public String getDES_PERFIL() {
        return DES_PERFIL;
    }

    public void setDES_PERFIL(String DES_PERFIL) {
        this.DES_PERFIL = DES_PERFIL;
    }

    public String getCVE_PERFIL_PADRE() {
        return CVE_PERFIL_PADRE;
    }

    public void setCVE_PERFIL_PADRE(String CVE_PERFIL_PADRE) {
        this.CVE_PERFIL_PADRE = CVE_PERFIL_PADRE;
    }

    public Date getFEC_ALT() {
        return FEC_ALT;
    }

    public void setFEC_ALT(Date FEC_ALT) {
        this.FEC_ALT = FEC_ALT;
    }

    public String getCVE_USU_ALTA() {
        return CVE_USU_ALTA;
    }

    public void setCVE_USU_ALTA(String CVE_USU_ALTA) {
        this.CVE_USU_ALTA = CVE_USU_ALTA;
    }

    public Date getFEC_BAJ() {
        return FEC_BAJ;
    }

    public void setFEC_BAJ(Date FEC_BAJ) {
        this.FEC_BAJ = FEC_BAJ;
    }

    public String getCVE_USU_BAJ() {
        return CVE_USU_BAJ;
    }

    public void setCVE_USU_BAJ(String CVE_USU_BAJ) {
        this.CVE_USU_BAJ = CVE_USU_BAJ;
    }

    public Date getFEC_ULT_MOD() {
        return FEC_ULT_MOD;
    }

    public void setFEC_ULT_MOD(Date FEC_ULT_MOD) {
        this.FEC_ULT_MOD = FEC_ULT_MOD;
    }

    public String getCVE_USUA() {
        return CVE_USUA;
    }

    public void setCVE_USUA(String CVE_USUA) {
        this.CVE_USUA = CVE_USUA;
    }

    public String getCVE_SERVICIOS() {
        return CVE_SERVICIOS;
    }

    public void setCVE_SERVICIOS(String CVE_SERVICIOS) {
        this.CVE_SERVICIOS = CVE_SERVICIOS;
    }
    
    
}
