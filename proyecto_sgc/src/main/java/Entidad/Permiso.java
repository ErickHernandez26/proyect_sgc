/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

/**
 *
 * @author ErickVonVilthem
 */
public class Permiso {
    
    String CVE_OPCION;
    String CVE_PERFIL;
    String ALT_PERMISO;
    String MOD_PERMISO;
    String BAJ_PERMISO;
    String REP_PERMISO;

    public Permiso() {
    }

    public Permiso(String CVE_OPCION, String CVE_PERFIL, String ALT_PERMISO, String MOD_PERMISO, String BAJ_PERMISO, String REP_PERMISO) {
        this.CVE_OPCION = CVE_OPCION;
        this.CVE_PERFIL = CVE_PERFIL;
        this.ALT_PERMISO = ALT_PERMISO;
        this.MOD_PERMISO = MOD_PERMISO;
        this.BAJ_PERMISO = BAJ_PERMISO;
        this.REP_PERMISO = REP_PERMISO;
    }

    public String getCVE_OPCION() {
        return CVE_OPCION;
    }

    public void setCVE_OPCION(String CVE_OPCION) {
        this.CVE_OPCION = CVE_OPCION;
    }

    public String getCVE_PERFIL() {
        return CVE_PERFIL;
    }

    public void setCVE_PERFIL(String CVE_PERFIL) {
        this.CVE_PERFIL = CVE_PERFIL;
    }

    public String getALT_PERMISO() {
        return ALT_PERMISO;
    }

    public void setALT_PERMISO(String ALT_PERMISO) {
        this.ALT_PERMISO = ALT_PERMISO;
    }

    public String getMOD_PERMISO() {
        return MOD_PERMISO;
    }

    public void setMOD_PERMISO(String MOD_PERMISO) {
        this.MOD_PERMISO = MOD_PERMISO;
    }

    public String getBAJ_PERMISO() {
        return BAJ_PERMISO;
    }

    public void setBAJ_PERMISO(String BAJ_PERMISO) {
        this.BAJ_PERMISO = BAJ_PERMISO;
    }

    public String getREP_PERMISO() {
        return REP_PERMISO;
    }

    public void setREP_PERMISO(String REP_PERMISO) {
        this.REP_PERMISO = REP_PERMISO;
    }
    
    
}
