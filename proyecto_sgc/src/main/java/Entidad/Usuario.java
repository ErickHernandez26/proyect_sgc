/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidad;

import java.sql.Date;

/**
 *
 * @author ErickVonVilthem
 */
public class Usuario {
   String CVE_USUA;
   String NOM_USUA;
   String APP_USUA;
   String APM_USUA;
   String RFC_USUA;
   String DES_PUESTO;
   String PASSWORD_USUA;
   String EMAIL_USUA;
   String TEL_USUA;
   String NUM_EMPLEADO_USUA;
   Date FECH_ALT;
   Date FECH_BAJ;
   Date FECH_ULT_MOD;
   String CVE_PERFIL;
   int PID_USUA;
   char CON_USUA;
   int PROVEEDOR;

    public Usuario() {
    }

    public Usuario(String CVE_USUA, String NOM_USUA, String APP_USUA, String APM_USUA, String RFC_USUA, String DES_PUESTO, String PASSWORD_USUA, String EMAIL_USUA, String TEL_USUA, String NUM_EMPLEADO_USUA, Date FECH_ALT, Date FECH_BAJ, Date FECH_ULT_MOD, String CVE_PERFIL, int PID_USUA, char CON_USUA, int PROVEEDOR) {
        this.CVE_USUA = CVE_USUA;
        this.NOM_USUA = NOM_USUA;
        this.APP_USUA = APP_USUA;
        this.APM_USUA = APM_USUA;
        this.RFC_USUA = RFC_USUA;
        this.DES_PUESTO = DES_PUESTO;
        this.PASSWORD_USUA = PASSWORD_USUA;
        this.EMAIL_USUA = EMAIL_USUA;
        this.TEL_USUA = TEL_USUA;
        this.NUM_EMPLEADO_USUA = NUM_EMPLEADO_USUA;
        this.FECH_ALT = FECH_ALT;
        this.FECH_BAJ = FECH_BAJ;
        this.FECH_ULT_MOD = FECH_ULT_MOD;
        this.CVE_PERFIL = CVE_PERFIL;
        this.PID_USUA = PID_USUA;
        this.CON_USUA = CON_USUA;
        this.PROVEEDOR = PROVEEDOR;
    }

    public String getCVE_USUA() {
        return CVE_USUA;
    }

    public void setCVE_USUA(String CVE_USUA) {
        this.CVE_USUA = CVE_USUA;
    }

    public String getNOM_USUA() {
        return NOM_USUA;
    }

    public void setNOM_USUA(String NOM_USUA) {
        this.NOM_USUA = NOM_USUA;
    }

    public String getAPP_USUA() {
        return APP_USUA;
    }

    public void setAPP_USUA(String APP_USUA) {
        this.APP_USUA = APP_USUA;
    }

    public String getAPM_USUA() {
        return APM_USUA;
    }

    public void setAPM_USUA(String APM_USUA) {
        this.APM_USUA = APM_USUA;
    }

    public String getRFC_USUA() {
        return RFC_USUA;
    }

    public void setRFC_USUA(String RFC_USUA) {
        this.RFC_USUA = RFC_USUA;
    }

    public String getDES_PUESTO() {
        return DES_PUESTO;
    }

    public void setDES_PUESTO(String DES_PUESTO) {
        this.DES_PUESTO = DES_PUESTO;
    }

    public String getPASSWORD_USUA() {
        return PASSWORD_USUA;
    }

    public void setPASSWORD_USUA(String PASSWORD_USUA) {
        this.PASSWORD_USUA = PASSWORD_USUA;
    }

    public String getEMAIL_USUA() {
        return EMAIL_USUA;
    }

    public void setEMAIL_USUA(String EMAIL_USUA) {
        this.EMAIL_USUA = EMAIL_USUA;
    }

    public String getTEL_USUA() {
        return TEL_USUA;
    }

    public void setTEL_USUA(String TEL_USUA) {
        this.TEL_USUA = TEL_USUA;
    }

    public String getNUM_EMPLEADO_USUA() {
        return NUM_EMPLEADO_USUA;
    }

    public void setNUM_EMPLEADO_USUA(String NUM_EMPLEADO_USUA) {
        this.NUM_EMPLEADO_USUA = NUM_EMPLEADO_USUA;
    }

    public Date getFECH_ALT() {
        return FECH_ALT;
    }

    public void setFECH_ALT(Date FECH_ALT) {
        this.FECH_ALT = FECH_ALT;
    }

    public Date getFECH_BAJ() {
        return FECH_BAJ;
    }

    public void setFECH_BAJ(Date FECH_BAJ) {
        this.FECH_BAJ = FECH_BAJ;
    }

    public Date getFECH_ULT_MOD() {
        return FECH_ULT_MOD;
    }

    public void setFECH_ULT_MOD(Date FECH_ULT_MOD) {
        this.FECH_ULT_MOD = FECH_ULT_MOD;
    }

    public String getCVE_PERFIL() {
        return CVE_PERFIL;
    }

    public void setCVE_PERFIL(String CVE_PERFIL) {
        this.CVE_PERFIL = CVE_PERFIL;
    }

    public int getPID_USUA() {
        return PID_USUA;
    }

    public void setPID_USUA(int PID_USUA) {
        this.PID_USUA = PID_USUA;
    }

    public char getCON_USUA() {
        return CON_USUA;
    }

    public void setCON_USUA(char CON_USUA) {
        this.CON_USUA = CON_USUA;
    }

    public int getPROVEEDOR() {
        return PROVEEDOR;
    }

    public void setPROVEEDOR(int PROVEEDOR) {
        this.PROVEEDOR = PROVEEDOR;
    }
   
}
