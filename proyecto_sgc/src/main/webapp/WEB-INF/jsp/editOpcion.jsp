<%-- 
    Document   : editOpcion
    Created on : 25/03/2019, 01:02:38 AM
    Author     : ErickVonVilthem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>editar</title>
    </head>
    <body>
       <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Actualizar registro</h4>
                </div>
                <div class="card-body">
                    
                    <form method="POST">
                        <label>Clave Opcion</label>
                            <input type="text" name="CVE_OPCION" class="form-control" value="${lista[0].CVE_OPCION}" disable="true">
                        <label>Descripcion Opcion</label>
                            <input type="text" name="DES_OPCION" class="form-control" value="${lista[0].DES_OPCION}">
                        <label>Usuario Alta</label>                        
                            <input type="text" name="USUARIO_ALTA" class="form-control" value="${lista[0].USUARIO_ALTA}">
                        <label>Usuario Bajao</label>                        
                            <input type="text" name="USUARIO_BAJA" class="form-control" value="${lista[0].USUARIO_BAJA}">
                        <label>Usuario Modifico</label>                        
                            <input type="text" name="USUARIO_MOD" class="form-control" value="${lista[0].USUARIO_MOD}">
                        <label>Fecha de alta</label>
                            <input type="text" name="FECHA_ALTA" class="form-control" value="${lista[0].FECHA_ALTA}">
                        <label>Fecha de Baja</label>
                            <input type="text" name="FECHA_BAJA" class="form-control" value="${lista[0].FECHA_BAJA}">
                        <label>Fecha de Modificacion</label>
                            <input type="text" name="FECHA_MOD" class="form-control" value="${lista[0].FECHA_MOD}">
                        
                        <input type="submit" value="Actualiar" class="btn btn-success">
                        <a href="listOpcion.htm">Regresar</a>                        
                    </form>
                </div>                      
            </div>            
        </div>
    </body>
</html>


