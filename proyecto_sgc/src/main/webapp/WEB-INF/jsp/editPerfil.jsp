<%-- 
    Document   : editPerfil
    Created on : 25/03/2019, 12:36:38 AM
    Author     : ErickVonVilthem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>editar</title>
    </head>
    <body>
       <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Actualizar registro</h4>
                </div>
                <div class="card-body">
                    
                    <form method="POST">
                        <label>Clave Perfil</label>
                            <input type="text" name="CVE_PERFIL" class="form-control" value="${lista[0].CVE_PERFIL}" disable="true">
                        <label>Descripcion Perfil</label>
                            <input type="text" name="DES_PERFIL" class="form-control" value="${lista[0].DES_PERFIL}">
                        <label>Clave Perfil Padre</label>
                            <input type="text" name="CVE_PERFIL_PADRE" class="form-control" value="${lista[0].CVE_PERFIL_PADRE}">
                        <label>Fecha de Alta</label>
                            <input type="text" name="FEC_ALTA" class="form-control" value="${lista[0].FEC_ALT}">
                        <label>Clave Usuario Alta</label>
                            <input type="text" name="CVE_US_ALT" class="form-control" value="${lista[0].CVE_USU_ALT}">
                        <label>Fecha de Baja</label>
                            <input type="text" name="FEC_BAJA" class="form-control" value="${lista[0].FEC_BAJ}">
                        <label>Clave Usuario baja</label>
                            <input type="text" name="CVE_USU_BAJ" class="form-control" value="${lista[0].CVE_USU_BAJ}">
                        <label>Fecha de Ultima Modificacion</label>
                            <input type="text" name="FEC_ULT_MOD" class="form-control" value="${lista[0].FEC_ULT_MOD}">
                        <label>Clave Perfil</label>
                            <input type="text" name="CVE_USUA" class="form-control" value="${lista[0].CVE_USUA}">
                        <label>Servicios</label>
                            <input type="checkbox" name="CVE_SERVICIOS" class="form-control">
                        
                        <input type="submit" value="Actualiar" class="btn btn-success">
                        <a href="listPerfil.htm">Regresar</a>                        
                    </form>
                </div>                      
            </div>            
        </div>
    </body>
</html>


