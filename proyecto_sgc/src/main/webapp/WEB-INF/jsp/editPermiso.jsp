<%-- 
    Document   : editPermiso
    Created on : 25/03/2019, 12:54:05 AM
    Author     : ErickVonVilthem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>editar</title>
    </head>
    <body>
       <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Actualizar registro</h4>
                </div>
                <div class="card-body">
                    
                    <form method="POST">
                        <label>Clave Permiso</label>
                            <input type="text" name="CVE_PERMISO" class="form-control" value="${lista[0].CVE_PERMISO}" disable="true">
                        <label>Clave Perfil</label>
                            <input type="text" name="CVE_PERFIL" class="form-control" value="${lista[0].CVE_PERFIL}">
                        <label>Alta Permiso</label>
                            <input type="checkbox" name="ALT_PERMISO" class="form-control">
                        <label>Baja Permiso</label>
                            <input type="checkbox" name="BAJ_PERMISO" class="form-control">
                        <label>Modificacion Permiso</label>
                            <input type="checkbox" name="MOD_PERMISO" class="form-control">
                        <label>Reporte Permiso</label>                        
                            <input type="checkbox" name="REP_PERMISO" class="form-control">
                        
                        <input type="submit" value="Actualiar" class="btn btn-success">
                        <a href="listPermiso.htm">Regresar</a>                        
                    </form>
                </div>                      
            </div>            
        </div>
    </body>
</html>



