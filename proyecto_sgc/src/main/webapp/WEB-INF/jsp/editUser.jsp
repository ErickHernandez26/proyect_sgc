<%-- 
    Document   : editUser
    Created on : 22/03/2019, 02:31:20 PM
    Author     : ErickVonVilthem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>editar</title>
    </head>
    <body>
       <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Actualizar registro</h4>
                </div>
                <div class="card-body">
                    
                    <form method="POST">                    
                        <label>Clave Usuario</label>
                            <input type="text" name=CVE_USUA class="form-control" value="${lista[0].CVE_USUA}" disable="true">
                        <label>Nombre_usuario</label>
                            <input type="text" name="NOM_USUA" class="form-control" value="${lista[0].NOM_USUA}">
                        <label>Apellido Paterno</label>
                            <input type="text" name="APP_USUA" class="form-control" value="${lista[0].APP_USUA}">
                        <label>Apellido Materno</label>
                            <input type="text" name="APM_USUA" class="form-control" value="${lista[0].APM_USUA}">
                        <label>RFC</label>
                            <input type="text" name="RFC_USUA" class="form-control" value="${lista[0].RFC_USUA}">
                        <label>Descripcion Puesto</label>
                            <input type="text" name="DES_PUESTO" class="form-control" value="${lista[0].DES_PUESTO}">
                        <label>Password</label>
                            <input type="text" name="PASSWORD_USUA" class="form-control" value="${lista[0].PASSWORD_USUA}">
                        <label>Email</label>
                            <input type="text" name="EMAIL_USUA" class="form-control" value="${lista[0].EMAIL_USUA}">
                        <label>Telefono</label>
                            <input type="text" name="TEL_USUA" class="form-control" value="${lista[0].TEL_USUA}">
                        <label>Numero de Empleado</label>
                            <input type="text" name="NUM_EMPLEADO_USUA" class="form-control" value="${lista[0].NUM_EMPLEADO_USUA}">
                        <label>Fecha de Alta</label>
                            <input type="text" name="FEC_ALTA" class="form-control" value="${lista[0].FEC_ALTA}">
                        <label>Fecha de Baja</label>
                            <input type="text" name="FEC_BAJA" class="form-control" value="${lista[0].FEC_BAJA}">
                        <label>Fecha de Ultima Modificacion</label>
                            <input type="text" name="FEC_ULT_MOD" class="form-control" value="${lista[0].FEC_ULT_MOD}">
                        <label>Clave Perfil</label>
                            <input type="text" name="CVE_PERFIL" class="form-control" value="${lista[0].CVE_PERFIL}">
                        <label>Codigo de Sesion</label>
                            <input type="text" name="PID_USUA" class="form-control" value="${lista[0].PID_USUA}">
                        <label>CON Usuario</label>
                            <input type="text" name="CON_USUA" class="form-control" value="${lista[0].CON_USUA}">
                        <label>Proveedor</label>
                            <select name="PROVEEDOR" class="form-control">
                                <option value="">...</option>
                            </select>
                        
                        <input type="submit" value="Actualiar" class="btn btn-success">
                        <a href="index.htm">Regresar</a>                        
                    </form>
                </div>                      
            </div>            
        </div>
    </body>
</html>

