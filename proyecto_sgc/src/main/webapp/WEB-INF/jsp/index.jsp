<%-- 
    Document   : listUser
    Created on : 22/03/2019, 02:30:25 PM
    Author     : ErickVonVilthem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>crud</title>
    </head>

    <body>
        <div class="conteiner mt-4">
            <div class="card-header bg-info text-white">
                <a class="btn btn-light" href="agregar.htm">Nuevo Registro</a>
                
            </div>
            
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>CLAVE_USUARO</th>
                            <th>NOMBRE_USUARIO</th>
                            <th>APP_USUA</th>
                            <th>APM_USUA</th>
                            <th>RFC_USUA</th>
                            <th>DES_PUESTO</th>
                            <th>PASSWORD</th>
                            <th>EMAIL</th>
                            <th>TELEFONO</th>
                            <th>NUM_EMPLREADO_USUA</th>
                            <th>FECHA_ALTA</th>
                            <th>FECHA_BAJA</th>
                            <th>FECHA_ULTIMA_MOD</th>
                            <th>PERFIL</th>
                            <th>PID_USUA</th>
                            <th>CON_USUA</th>
                            <th>PROVEEDOR</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="user" items="${lista}">
                        <tr>
                            <td>${user.CVE_USUA}</td>
                            <td>${user.NOM_USUA}</td>
                            <td>${user.APP_USUA}</td>
                            <td>${user.APM_USUA}</td>
                            <td>${user.RFC_USUA}</td>
                            <td>${user.DES_PUESTO}</td>
                            <td>${user.PASSWORD_USUA}</td>
                            <td>${user.EMAIL_USUA}</td>
                            <td>${user.TEL_USUA}</td>
                            <td>${user.NUM_EMPLEADO_USUA}</td>
                            <td>${user.FEC_ALTA}</td>
                            <td>${user.FEC_BAJA}</td>
                            <td>${user.FEC_ULT_MOD}</td>
                            <td>${user.CVE_PERFIL}</td>
                            <td>${user.PID_USUA}</td>
                            <td>${user.CON_USUA}</td>
                            <td>${user.PROVEEDOR}</td>
                            <td>
                                <a href="editar.htm?CVE_USER=${user.CVE_USER}" class="btn btn-warning" >Editar</a> 
                                <a href="delete.htm?CVE_USER=${USER.CVE_USER}" class="btn btn-danger">Delete</a>                                 
                            </td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>            
        </div>
    </body>
</html>

