<%-- 
    Document   : listOpcion
    Created on : 25/03/2019, 01:02:50 AM
    Author     : ErickVonVilthem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>crud</title>
    </head>

    <body>
        <div class="conteiner mt-4">
            <div class="card-header bg-info text-white">
                <a class="btn btn-light" href="agregar.htm">Nuevo Registro</a>
                
            </div>
            
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>CLAVE OPCION</th>
                            <th>DESCRIPCION OPCION</th>                            
                            <th>USUARIO ALTA</th>
                            <th>USUARIO BAJA</th>
                            <th>USUARIO OD</th>
                            <th>FECHA ALTA</th>
                            <th>FECHA BAJA</th>
                            <th>FECHA MODIFICACION</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="opcion" items="${lista}">
                        <tr>
                            <td>${opcion.CVE_OPCION}</td>
                            <td>${opcion.DES_OPCION}</td>
                            <td>${opcion.USUARIO_ALTA}</td>
                            <td>${opcion.USUARIO_BAJA}</td>
                            <td>${opcion.USUARIO_OD}</td>
                            <td>${opcion.FECHA_ALTA}</td>
                            <td>${opcion.FECHA_BAJA}</td>
                            <td>${opcion.FECHA_MOD}</td>
                            <td>
                                <a href="editar.htm?CVE_USER=${opcion.CVE_OPCION}" class="btn btn-warning" >Editar</a> 
                                <a href="delete.htm?CVE_USER=${opcion.CVE_OPCION}" class="btn btn-danger">Delete</a>                                 
                            </td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>            
        </div>
    </body>
</html>


