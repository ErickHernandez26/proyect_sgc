<%-- 
    Document   : listPerfil
    Created on : 25/03/2019, 12:36:51 AM
    Author     : ErickVonVilthem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>crud</title>
    </head>

    <body>
        <div class="conteiner mt-4">
            <div class="card-header bg-info text-white">
                <a class="btn btn-light" href="agregar.htm">Nuevo Registro</a>
                
            </div>
            
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>PERFIL</th>
                            <th>DESCRIPCION PERFIL</th>
                            <th>CLAVE PERFIL PADRE</th>                            
                            <th>FECHA_ALTA</th>
                            <th>CLAVE_USUA_ALTA</th>
                            <th>FECHA_BAJA</th>
                            <th>CLAVE_USUA_BAJA</th>
                            <th>FECHA_ULTIMA_MOD</th>
                            <th>CLAVE_USUARO</th>
                            <th>SERVICIOS</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="perfil" items="${lista}">
                        <tr>
                            <td>${perfil.CVE_PERFIL}</td>
                            <td>${perfil.DES_PERFIL}</td>
                            <td>${perfil.CVE_PERFIL_PADRE}</td>
                            <td>${perfil.FEC_ALT}</td>
                            <td>${perfil.CVE_USU_ALT}</td>
                            <td>${perfil.FEC_BAJ}</td>
                            <td>${perfil.CVE_USU_BAJ}</td>
                            <td>${perfil.FEC_ULT_MOD}</td>                            
                            <td>${perfil.CVE_USUA}</td>
                            <td>${perfil.CVE_SERVICIOS}</td>
                            <td>
                                <a href="editar.htm?CVE_USER=${perfil.CVE_PERFIL}" class="btn btn-warning" >Editar</a> 
                                <a href="delete.htm?CVE_USER=${perfil.CVE_PERFIL}" class="btn btn-danger">Delete</a>                                 
                            </td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>            
        </div>
    </body>
</html>

