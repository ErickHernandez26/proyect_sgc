<%-- 
    Document   : listPermiso
    Created on : 25/03/2019, 12:54:15 AM
    Author     : ErickVonVilthem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>crud</title>
    </head>

    <body>
        <div class="conteiner mt-4">
            <div class="card-header bg-info text-white">
                <a class="btn btn-light" href="agregar.htm">Nuevo Registro</a>
                
            </div>
            
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>CLAVE PERMISO</th>
                            <th>CLAVE PERFIL</th>                            
                            <th>ALT_PERMISO</th>
                            <th>BAJ_PERMISO</th>
                            <th>MOD_PERMISO</th>
                            <th>REP_PERMISO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="permiso" items="${lista}">
                        <tr>
                            <td>${permiso.CVE_PERMISO}</td>
                            <td>${permiso.CVE_PERFIL}</td>
                            <td>${permiso.ALT_PERMISO}</td>
                            <td>${permiso.BAJ_PERMISO}</td>
                            <td>${permiso.MOD_PERMISO}</td>
                            <td>${permiso.REP_PERMISO}</td>
                            <td>
                                <a href="editar.htm?CVE_USER=${permiso.CVE_PERMISO}" class="btn btn-warning" >Editar</a> 
                                <a href="delete.htm?CVE_USER=${permiso.CVE_PERMISO}" class="btn btn-danger">Delete</a>                                 
                            </td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>            
        </div>
    </body>
</html>

