<%-- 
    Document   : newOpcion
    Created on : 25/03/2019, 01:02:27 AM
    Author     : ErickVonVilthem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>agregar</title>
    </head>
    <body>        
        <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Agregar nuevo registro</h4>
                </div>
                <div class="card-body">                    
                    <form method="POST">
                        <label>Clave Opcion</label>
                            <input type="text" name="CVE_OPCION" class="form-control">
                        <label>Descripcion Opcion</label>
                            <input type="text" name="DES_OPCION" class="form-control">
                        <label>Usuario Alta</label>                        
                            <input type="text" name="USUARIO_ALTA" class="form-control">
                        <label>Usuario Bajao</label>                        
                            <input type="text" name="USUARIO_BAJA" class="form-control">
                        <label>Usuario Modifico</label>                        
                            <input type="text" name="USUARIO_MOD" class="form-control">
                        <label>Fecha de alta</label>
                            <input type="text" name="FECHA_ALTA" class="form-control">
                        <label>Fecha de Baja</label>
                            <input type="text" name="FECHA_BAJA" class="form-control">
                        <label>Fecha de Modificacion</label>
                            <input type="text" name="FECHA_MOD" class="form-control">
                        
                        
                        <input type="submit" value="agregar" class="btn btn-success">
                        <a href="listOpcion.htm">Regresar</a>                                               
                    </form>
                </div>                
            </div>            
        </div>        
    </body>
</html>

