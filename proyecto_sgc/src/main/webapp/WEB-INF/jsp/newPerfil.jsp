<%-- 
    Document   : newPerfil
    Created on : 25/03/2019, 12:36:28 AM
    Author     : ErickVonVilthem
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>agregar</title>
    </head>
    <body>        
        <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Agregar nuevo registro</h4>
                </div>
                <div class="card-body">                    
                    <form method="POST">
                        <label>Clave Perfil</label>
                            <input type="text" name="CVE_PERFIL" class="form-control">
                        <label>Descripcion Perfil</label>
                            <input type="text" name="DES_PERFIL" class="form-control">
                        <label>Clave Perfil Padre</label>
                            <input type="text" name="CVE-PERFIL_PADRE" class="form-control">
                        <label>Fecha de Alta</label>
                            <input type="text" name="FEC_ALT" class="form-control">
                        <label>Clave Usuario Alta</label>
                            <input type="text" name="CVE_USU_ALT" class="form-control">
                        <label>Fecha de Baja</label>
                            <input type="text" name="FEC_BAJ" class="form-control">
                        <label>Clave Usuario Baja</label>
                            <input type="text" name="CVE_USU_BAJ" class="form-control">
                        <label>Fecha de Ultima Modificacion</label>
                            <input type="text" name="FEC_ULT_MOD" class="form-control">
                        <label>Clave Usuario</label>
                            <input type="text" name="CVE_USUA" class="form-control">
                        <label>Servicios</label>
                        <input type="checkbox" name="CVE_SERVICIOS" class="form-control">
                        <input type="submit" value="agregar" class="btn btn-success">
                        <a href="listPerfil.htm">Regresar</a>                                               
                    </form>
                </div>                
            </div>            
        </div>        
    </body>
</html>

