<%-- 
    Document   : NewUser
    Created on : 22/03/2019, 02:29:48 PM
    Author     : ErickVonVilthem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>agregar</title>
    </head>
    <body>        
        <div class="container mt-4 col-lg-4">
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Agregar nuevo registro</h4>
                </div>
                <div class="card-body">                    
                    <form method="POST">
                        <label>Clave Usuario</label>
                            <input type="text" name=CVE_USUA class="form-control">
                        <label>Nombre_usuario</label>
                            <input type="text" name="NOM_USUA" class="form-control">
                        <label>Apellido Paterno</label>
                            <input type="text" name="APP_USUA" class="form-control">
                        <label>Apellido Materno</label>
                            <input type="text" name="APM_USUA" class="form-control">
                        <label>RFC</label>
                            <input type="text" name="RFC_USUA" class="form-control">
                        <label>Descripcion Puesto</label>
                            <input type="text" name="DES_PUESTO" class="form-control">
                        <label>Password</label>
                            <input type="text" name="PASSWORD_USUA" class="form-control">
                        <label>Email</label>
                            <input type="text" name="EMAIL_USUA" class="form-control">
                        <label>Telefono</label>
                            <input type="text" name="TEL_USUA" class="form-control">
                        <label>Numero de Empleado</label>
                            <input type="text" name="NUM_EMPLEADO_USUA" class="form-control">
                        <label>Fecha de Alta</label>
                            <input type="text" name="FEC_ALTA" class="form-control">
                        <label>Fecha de Baja</label>
                            <input type="text" name="FEC_BAJA" class="form-control">
                        <label>Fecha de Ultima Modificacion</label>
                            <input type="text" name="FEC_ULT_MOD" class="form-control">
                        <label>Clave Perfil</label>
                            <input type="text" name="CVE_PERFIL" class="form-control">
                        <label>Codigo de Sesion</label>
                            <input type="text" name="PID_USUA" class="form-control">
                        <label>CON Usuario</label>
                            <input type="text" name="CON_USUA" class="form-control">
                        <label>Proveedor</label>
                            <select name="PROVEEDOR" class="form-control">
                                <option>...</option>
                            </select>
                        <input type="submit" value="agregar" class="btn btn-success">
                        <a href="index.htm">Regresar</a>                                               
                    </form>
                </div>                
            </div>            
        </div>        
    </body>
</html>
